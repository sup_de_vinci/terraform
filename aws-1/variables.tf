variable "instance_name" {
  description = "Value name of the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstanceVianney"
}