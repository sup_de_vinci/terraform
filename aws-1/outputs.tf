output "private_ip" {
  description = "private ip of the instance"
  value       = aws_instance.app_server.private_ip
}

output "instance_id" {
  description = "ID of EC2 instance"
  value       = aws_instance.app_server.id
}